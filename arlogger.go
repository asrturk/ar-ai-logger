package arlogger

import (
	"bytes"
	"fmt"
	"io"
	"mime/multipart"
	"net/http"
	"os"
	"path/filepath"
	"runtime"
	"time"

	rotatelogs "github.com/lestrrat-go/file-rotatelogs"
	logrus "github.com/sirupsen/logrus"
)

type ArLoggerStruct struct {
	Log                  *logrus.Logger
	serverURLLog         string
	serverURLDailyBackup string
	debugFlag            bool
	publishLogsFlag      bool
	logFileNameParam     string
}

func (Log ArLoggerStruct) Fatal(v ...interface{}) {

	log := Log.Log

	pc, file, line, _ := runtime.Caller(1)

	dt := time.Now()
	dtString := dt.Format("01-02-2006 15:04:05")

	var entry = Log.Log.WithField("func", runtime.FuncForPC(pc).Name()).WithField("line", line).WithField("message", fmt.Sprintf("%v", v)).WithField("pathname", file).WithField("levelname", "fatal").WithField("projectname", Log.logFileNameParam).WithField("time", dtString)

	if Log.debugFlag == true {
		entry.Fatal(v...)
		return
	}

	byts, err := entry.Bytes()
	if err != nil {
		log.Warn(err)
		return
	}

	_, err = http.Post(Log.serverURLLog, "application/json", bytes.NewBuffer(byts))
	if err != nil {
		log.Warn(err)
		return
	}

	entry.Fatal(v...)
}

func (Log ArLoggerStruct) Error(v ...interface{}) {

	log := Log.Log

	pc, file, line, _ := runtime.Caller(1)

	dt := time.Now()
	dtString := dt.Format("01-02-2006 15:04:05")

	var entry = Log.Log.WithField("func", runtime.FuncForPC(pc).Name()).WithField("line", line).WithField("message", fmt.Sprintf("%v", v)).WithField("pathname", file).WithField("levelname", "error").WithField("projectname", Log.logFileNameParam).WithField("time", dtString)

	if Log.debugFlag == true {
		entry.Error(v...)
		return
	}

	byts, err := entry.Bytes()
	if err != nil {
		log.Warn(err)
		return
	}

	_, err = http.Post(Log.serverURLLog, "application/json", bytes.NewBuffer(byts))
	if err != nil {
		log.Warn(err)
		return
	}

	entry.Error(v...)
}

func (Log ArLoggerStruct) Warn(v ...interface{}) {

	log := Log.Log

	pc, file, line, _ := runtime.Caller(1)
	var entry = log.WithField("func", runtime.FuncForPC(pc).Name()).WithField("line", line).WithField("message", v).WithField("pathname", file).WithField("levelname", "warn")

	entry.Warn(v...)
}

func (Log ArLoggerStruct) Info(v ...interface{}) {

	log := Log.Log

	pc, file, line, _ := runtime.Caller(1)
	var entry = log.WithField("func", runtime.FuncForPC(pc).Name()).WithField("line", line).WithField("message", v).WithField("pathname", file).WithField("levelname", "info")

	entry.Info(v...)
}

func (Log ArLoggerStruct) Debug(v ...interface{}) {

	log := Log.Log

	pc, file, line, _ := runtime.Caller(1)
	var entry = log.WithField("func", runtime.FuncForPC(pc).Name()).WithField("line", line).WithField("message", v).WithField("pathname", file).WithField("levelname", "debug")

	entry.Debug(v...)
}

func (Log ArLoggerStruct) Trace(v ...interface{}) {

	log := Log.Log

	pc, file, line, _ := runtime.Caller(1)
	var entry = log.WithField("func", runtime.FuncForPC(pc).Name()).WithField("line", line).WithField("message", v).WithField("pathname", file).WithField("levelname", "trace")

	entry.Trace(v...)
}

// init ile değiştir
func SetURL(url string, logFileName string, flags ...bool) ArLoggerStruct {

	var InitLogger ArLoggerStruct = ArLoggerStruct{}

	InitLogger.serverURLLog = url + "/log"
	InitLogger.serverURLDailyBackup = url + "/daily"

	if logFileName == "" {
		InitLogger.logFileNameParam = "ArLogger"
	} else {
		InitLogger.logFileNameParam = logFileName
	}

	NewLogger := logrus.New()
	NewLogger.SetFormatter(&logrus.JSONFormatter{
		FieldMap: logrus.FieldMap{
			logrus.FieldKeyLevel: "levelname",
		},
	})

	if len(flags) > 0 {
		InitLogger.debugFlag = flags[0]
		InitLogger.publishLogsFlag = flags[1]
	}

	if InitLogger.debugFlag == true {
		InitLogger.Log = NewLogger
		return InitLogger
	}

	path := fmt.Sprintf("./%s", logFileName)
	writer, err := rotatelogs.New(
		fmt.Sprintf("%s_%s", path, "%Y-%m-%d.%H:%M:%S.log"),
		rotatelogs.WithHandler(rotatelogs.HandlerFunc(func(e rotatelogs.Event) {
			if e.Type() != rotatelogs.FileRotatedEventType {
				return
			}
			fileName := (e.(*rotatelogs.FileRotatedEvent).PreviousFile())
			fileDir, err := os.Getwd()
			if err != nil {
				NewLogger.Error(err)
				return
			}
			filePath := filepath.Join(fileDir, fileName)

			file, err := os.Open(filePath)
			defer file.Close()
			if err != nil {
				NewLogger.Error(err)
				return
			}
			body := &bytes.Buffer{}
			writer := multipart.NewWriter(body)
			part, err := writer.CreateFormFile("file", filepath.Base(file.Name()))
			if err != nil {
				NewLogger.Error(err)
				return
			}
			io.Copy(part, file)
			writer.Close()

			r, err := http.NewRequest("POST", InitLogger.serverURLDailyBackup, body)
			if err != nil {
				NewLogger.Error(err)
				return
			}
			r.Header.Add("Content-Type", writer.FormDataContentType())
			client := &http.Client{}
			resp, err := client.Do(r)
			if err != nil {
				NewLogger.Error(err)
				return
			}
			fmt.Println(resp)
		})),
		rotatelogs.WithRotationTime(time.Hour*24),
		rotatelogs.WithMaxAge(time.Hour*24*7),
	)
	if err != nil {
		NewLogger.Warn(err)
	}
	mw := io.MultiWriter(writer, os.Stdout)
	NewLogger.SetOutput(mw)

	InitLogger.Log = NewLogger

	return InitLogger
}
